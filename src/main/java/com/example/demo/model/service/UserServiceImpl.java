package com.example.demo.model.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.model.dao.UserDao;
import com.example.demo.model.entity.User;

@Service
public class UserServiceImpl implements UserService{
	@Autowired private UserDao userDao;

	@Override
	@Transactional(readOnly = true)
	public List<User> listAll() {
		return userDao.findAll();
	}
	
	@Override
	@Transactional(readOnly = true)
	public Page<User> listByPage(Pageable pageable) {
		return userDao.findAll(pageable);
	}
	
	@Override
	@Transactional(readOnly = true)
	public User getById(Long id) {
		return userDao.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public User save(User user) {
		return userDao.save(user);
	}

	@Override
	@Transactional
	public void delete(Long id) {	
		userDao.deleteById(id);
	}


}




