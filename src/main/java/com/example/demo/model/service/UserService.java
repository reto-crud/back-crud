package com.example.demo.model.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.example.demo.model.entity.User;

public interface UserService {
	public List<User> listAll();
	public Page<User> listByPage(Pageable pageable);
	public User getById(Long id);
	public User save(User user);
	public void delete(Long id);
	
	
}
