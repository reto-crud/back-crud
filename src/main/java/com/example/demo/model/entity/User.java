package com.example.demo.model.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Entity
@Table(name="users")
public class User implements Serializable{
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
//	@Column(nullable = false,unique = true)
	private String uid;
	@NotEmpty(message = "no puede estar vacio")
	@Column(nullable = false,unique = true)
	private String username;
	@NotEmpty(message = "no puede estar vacio") 
	@Email(message = "no es una direccion de correo bien formada")
	@Column(nullable = false,unique = true)
	private String email;
	@NotEmpty(message = "no puede estar vacio")
	@Column(nullable = false)
	private String password;
	@NotEmpty(message = "no puede estar vacio")
	@Column(nullable = false)
	private String rol;
	@NotEmpty(message = "no puede estar vacio") 
	@Size(min = 4,max = 30,message = "el tamanio tiene que estar entre 4 y 12 caracteres")
	@Column(name="full_name",nullable = false)
	private String fullName;
	private String avatar;
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getUid() {
		return uid;
	}
	public void setUid(String uid) {
		this.uid = uid;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getRol() {
		return rol;
	}
	public void setRol(String rol) {
		this.rol = rol;
	}
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public String getAvatar() {
		return avatar;
	}
	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}
	@Override
	public String toString() {
		return "User [id=" + id + ", uid=" + uid + ", username=" + username + ", email=" + email + ", password="
				+ password + ", rol=" + rol + ", fullName=" + fullName + ", avatar=" + avatar + "]";
	}

	

}














